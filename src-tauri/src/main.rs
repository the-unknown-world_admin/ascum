// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]
// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
#[tauri::command]
fn capture(file: &str) {
    use screenshots::Screen;
    let screens = Screen::all().unwrap();
    for screen in screens {
        if !screen.display_info.is_primary {
            continue;
        }
        println!("capturer {screen:?}");
        let image = screen.capture().unwrap();
        image.save(file).unwrap();
    }
}

fn main() {
    use tauri::Manager;
    use tauri::{CustomMenuItem, SystemTray, SystemTrayEvent, SystemTrayMenu};
    let tray_menu = SystemTrayMenu::new()
        .add_item(CustomMenuItem::new("setting".to_string(), "系统设置"))
        .add_item(CustomMenuItem::new("quit".to_string(), "退出助手"));
    let system_tray = SystemTray::new().with_menu(tray_menu);
    tauri::Builder::default()
        .setup(|app| {
            let window = app.get_window("main").unwrap();

            #[cfg(debug_assertions)] // only include this code on debug builds
            {
                window.open_devtools();
            }
            #[cfg(windows)]
            {
                use windows::Win32::Foundation::HWND;
                let hwnd = window.hwnd().unwrap().0;
                let hwnd = HWND(hwnd);
                unsafe {
                    let mut style_ex = WINDOW_EX_STYLE(GetWindowLongW(hwnd, GWL_EXSTYLE) as u32);
                    style_ex |= WS_EX_APPWINDOW // for taskbar
                            | WS_EX_COMPOSITED
                            | WS_EX_LAYERED
                            | WS_EX_TRANSPARENT
                            | WS_EX_TOPMOST;
                    use windows::Win32::UI::WindowsAndMessaging::*;
                    let nindex = GWL_EXSTYLE;
                    let _pre_val = SetWindowLongA(hwnd, nindex, style_ex.0 as i32);
                }
            }
            window.set_ignore_cursor_events(true)?;
            window.set_skip_taskbar(true)?;
            window.set_always_on_top(true)?;
            window.maximize()?;

            Ok(())
        })
        .system_tray(system_tray)
        .on_system_tray_event(|app, event| match event {
            SystemTrayEvent::MenuItemClick { id, .. } => match id.as_str() {
                "quit" => {
                    std::process::exit(0);
                }
                "setting" => {
                    app.emit_to("main", "setting", ()).expect("can not emit to main window");
                }
                _ => {}
            },
            _ => {}
        })
        .invoke_handler(tauri::generate_handler![capture])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
