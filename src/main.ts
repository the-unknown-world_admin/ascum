import { dateZhCN, NConfigProvider, NGlobalStyle, NNotificationProvider, zhCN, NEl, darkTheme } from 'naive-ui'
import { createApp, h } from 'vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import App from './App.vue'

const routes = Object.values(import.meta.glob('./pages/*/router.ts', { eager: true })).map(m => (m as Any).default || m)

const router = createRouter({
  routes: [
    {
      path: '/:catchAll(.*)',
      redirect: '/404'
    },
    ...routes
  ],
  history: createWebHashHistory('./')
})
const app = createApp({
  render: () => {
    return h(NConfigProvider, {
      theme: darkTheme,
      locale: zhCN,
      dateLocale: dateZhCN,
      themeOverrides: {
        common: {
          // primaryColor: '#B59BFFFF',
          // primaryColorHover: '#C2ACFFFF',
          // primaryColorPressed: '#9E7CFFFF',
          // primaryColorSuppl: 'rgba(119, 77, 240, 1)',
          // infoColor: '#B59BFFFF',
          // infoColorHover: '#C2ACFFFF',
          // infoColorPressed: '#9E7CFFFF',
          // infoColorSuppl: 'rgba(119, 77, 240, 1)',
          // successColor: '#B59BFFFF',
          // successColorHover: '#C2ACFFFF',
          // successColorPressed: '#9E7CFFFF',
          // successColorSuppl: 'rgba(119, 77, 240, 1)',
          // tableColor: 'rgba(24, 23, 29, 1)',
          // modalColor: 'rgba(90, 90, 98, 1)'
        },
        Button: {
          // textColorPrimary: '#FFFFFFFF',
          // textColorGhost: 'rgba(255, 255, 255, 0.6)',
          // textColorHover: '#FFFFFFFF',
          // textColorPressed: '#9E7CFFFF',
          // textColorFocus: '#C2ACFFFF',
          // textColorTextDisabled: 'rgba(255, 255, 255, 0.82)',
          // textColorDisabled: 'rgba(255, 255, 255, 0.82)',
          // textColorText: 'rgba(255, 255, 255, 0.82)',
          // textColorTextHover: '#C2ACFFFF',
          // textColorTextPressed: '#9E7CFFFF',
          // textColorGhostDisabled: 'rgba(255, 255, 255, 0.82)',
          // colorPrimary: '#B59BFFFF',
          // borderPrimary: '1px solid #3C3654'
        },
        Table: {
          // borderColor: 'rgba(90, 90, 98, 1)'
        },
        Menu: {
          // itemTextColorActive: '#FFFFFFFF',
          // color: '#2F2D38FF',
          // itemColorActive: 'rgba(21, 20, 25, 1)',
          // itemTextColor: 'rgba(151, 150, 155, 1)',
          // itemIconColor: 'rgba(151, 150, 155, 0.9)'
        },
        Input: {
          // border: '1px solid #5A5A62',
          // countTextColorDisabled: 'rgba(255, 255, 255, 0.38)',
          // groupLabelTextColor: 'rgba(255, 255, 255, 0.82)',
          // color: 'rgba(37, 36, 43, 1)',
          // colorDisabled: 'rgba(54, 53, 62, 1)',
          // borderDisabled: '1px solid #5A5A62'
        }
      }
    }, {
      default: () => {
        return [h(NGlobalStyle), h(NNotificationProvider, {
          placement: 'bottom-right'
        }, {
          default: () => h(NEl, {
            style: {
              width: '100vw',
              height: '100vh'
            }
          }, {
            default: () => h(App, {
              routes,
              ref: 'root'
            })
          })
        })]
      }
    })
  }
}).use(router)
const vm = app.mount('#app')
app.config.errorHandler = (vm.$refs.root as Any).errorHandler || ((err: Error) => {
  console.error(`no error handler for ${err.message}`)
})

window.addEventListener('unhandledrejection', evt => {
  console.error(evt)
  evt.preventDefault()
  app.config.errorHandler?.(evt.reason, vm, '')
})
window.addEventListener('error', evt => {
  console.error(evt)
  evt.preventDefault()
  app.config.errorHandler?.(evt.error, vm, '')
})
