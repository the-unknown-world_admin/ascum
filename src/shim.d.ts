import { Component } from 'vue'
import 'vue-router'
export {}

declare module 'vue-router' {
  // eslint-disable-next-line no-unused-vars
  interface RouteMeta {
    /** 导航栏标题 */
    routeTitle?: string;
    /** 导航栏图标 */
    routeIcon?: Component;
    /** 导航菜单顺序 */
    routeOrder?: number;
  }
}
