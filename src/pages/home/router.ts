import { RouteRecordRaw } from 'vue-router'

export default {
  path: '/',
  name: 'home',
  component: () => import('./index.vue' as Any)
} as RouteRecordRaw
