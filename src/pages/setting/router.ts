import { RouteRecordRaw } from 'vue-router'

export default {
  path: '/setting',
  name: 'setting',
  component: () => import('./index.vue' as Any)
} as RouteRecordRaw
