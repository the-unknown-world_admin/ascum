import { RouteRecordRaw } from 'vue-router'

export default {
  path: '/map',
  name: 'map',
  component: () => import('./index.vue' as Any)
} as RouteRecordRaw
