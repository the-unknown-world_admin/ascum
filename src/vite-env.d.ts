/// <reference types="vite/client" />

/** 代指任意类型 请不要随意使用 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
declare type Any = any

declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, Any>
  export default component
}

// eslint-disable-next-line no-unused-vars
declare const host = {
  send (evt: string, ...args: Any[]) {},
  listen (evt: string, cb: (...args: Any[]) => void) {},
  postMessage (channel: string, message: Any, transfer?: MessagePort[]) {}
}

// eslint-disable-next-line no-unused-vars
declare const cv: Any

declare module './opencv.js'
